﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlueEstate.Models;
namespace BlueEstate.Models
{
    public class EvSlider
    {
        public List<Ev> evler;
        public List<Slider> slider;

        public EvSlider(BlueEstateDBEntities db)
        {
            evler = db.Ev.Take(12).ToList();
            slider = db.Slider.ToList();
        }
    }
}