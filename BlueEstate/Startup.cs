﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BlueEstate.Startup))]
namespace BlueEstate
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
