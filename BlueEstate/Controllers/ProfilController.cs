﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlueEstate.Models;

namespace BlueEstate.Controllers
{
    public class ProfilController : Controller
    {
        private BlueEstateDBEntities db = new BlueEstateDBEntities();

        // GET: Profil
        public ActionResult Index()
        {
            if (Session["id"] != null)
            {
                int id = Convert.ToInt32(Session["id"]);
                Uye uye = new Uye();
                uye = db.Uye.Find(id);
                return View(uye);
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Profil/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uye uye = db.Uye.Find(id);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }

        // GET: Profil/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Profil/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UyeID,KullaniciAdi,Sifresi,Email,UyelikTarihi,UyeAdi,Yetki")] Uye uye)
        {
            if (ModelState.IsValid)
            {
                db.Uye.Add(uye);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(uye);
        }

        // GET: Profil/Edit/5
        public ActionResult Edit()
        {

            if (Session["id"] != null)
            {


                Uye uye = db.Uye.Find(Session["id"]);

                return View(uye);
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Cikis()
        {

            Session.Clear();
            
            return RedirectToAction("Index", "Home");
        }

        // POST: Profil/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UyeID,KullaniciAdi,Sifresi,Email,UyelikTarihi,UyeAdi,Yetki")] Uye uye)
        {
            if (Session["id"] != null)
            {
                if (ModelState.IsValid)
                {


                    db.Entry(uye).State = EntityState.Modified;
                    db.SaveChanges();

                    Session["ad"] = uye.UyeAdi;
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Profil/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uye uye = db.Uye.Find(id);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }

        // POST: Profil/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Uye uye = db.Uye.Find(id);
            db.Uye.Remove(uye);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
