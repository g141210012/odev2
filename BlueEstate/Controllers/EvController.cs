﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlueEstate.Models;

namespace BlueEstate.Controllers
{
    public class EvController : Controller
    {
        private BlueEstateDBEntities db = new BlueEstateDBEntities();

        // GET: Ev
        public ActionResult Index()
        {
            var ev = db.Ev.Include(e => e.Sehir).Include(e => e.Uye);
            return View(ev.ToList());
        }

        // GET: Ev/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ev ev = db.Ev.Find(id);
            if (ev == null)
            {
                return HttpNotFound();
            }
            return View(ev);
        }

        // GET: Ev/Create
        public ActionResult Create()
        {
            ViewBag.SehirID = new SelectList(db.Sehir, "SehirID", "SehirAdi");
            ViewBag.UyeID = new SelectList(db.Uye, "UyeID", "KullaniciAdi");
            return View();
        }

        // POST: Ev/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EvID,Adres,IlanTarihi,EvinYasi,IlanTuru,KonutTipi,Fiyat,OdaSayisi,BanyoSayisi,Boyut,UyeID,SehirID")] Ev ev)
        {
            if (ModelState.IsValid)
            {
                db.Ev.Add(ev);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SehirID = new SelectList(db.Sehir, "SehirID", "SehirAdi", ev.SehirID);
            ViewBag.UyeID = new SelectList(db.Uye, "UyeID", "KullaniciAdi", ev.UyeID);
            return View(ev);
        }

        // GET: Ev/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ev ev = db.Ev.Find(id);
            if (ev == null)
            {
                return HttpNotFound();
            }
            ViewBag.SehirID = new SelectList(db.Sehir, "SehirID", "SehirAdi", ev.SehirID);
            ViewBag.UyeID = new SelectList(db.Uye, "UyeID", "KullaniciAdi", ev.UyeID);
            return View(ev);
        }

        // POST: Ev/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EvID,Adres,IlanTarihi,EvinYasi,IlanTuru,KonutTipi,Fiyat,OdaSayisi,BanyoSayisi,Boyut,UyeID,SehirID")] Ev ev)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ev).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SehirID = new SelectList(db.Sehir, "SehirID", "SehirAdi", ev.SehirID);
            ViewBag.UyeID = new SelectList(db.Uye, "UyeID", "KullaniciAdi", ev.UyeID);
            return View(ev);
        }

        // GET: Ev/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ev ev = db.Ev.Find(id);
            if (ev == null)
            {
                return HttpNotFound();
            }
            return View(ev);
        }

        // POST: Ev/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ev ev = db.Ev.Find(id);
            db.Ev.Remove(ev);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
