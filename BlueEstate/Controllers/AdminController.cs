﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlueEstate.Models;
using System.IO;

namespace BlueEstate.Controllers
{
    public class AdminController : Controller
    {
        private BlueEstateDBEntities db = new BlueEstateDBEntities();

        // GET: Admin
        public ActionResult Index()
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            int uye_id = int.Parse(Session["uye_id"].ToString());

            return View(db.Uye.FirstOrDefault(x => x.UyeID == uye_id));
        }

        public ActionResult EvListe()
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            var ev = db.Ev.Include(e => e.Sehir).Include(e => e.Uye);
            return View(ev.ToList());
        }

        // GET: Admin/Details/5
        public ActionResult EvDetay(int? id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ev ev = db.Ev.Find(id);
            if (ev == null)
            {
                return HttpNotFound();
            }
            return View(ev);
        }

        // GET: Admin/Create
        public ActionResult EvOlustur()
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            ViewBag.SehirID = new SelectList(db.Sehir, "SehirID", "SehirAdi");
            ViewBag.UyeID = new SelectList(db.Uye, "UyeID", "KullaniciAdi");
            return View();
        }

        // POST: Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EvOlustur(HttpPostedFileBase Fotograf2, [Bind(Include = "EvID,Aciklama,IlanTarihi,EvinYasi,IlanTuru,KonutTipi,Fiyat,OdaSayisi,BanyoSayisi,Boyut,UyeID,SehirID,Fotograf")] Ev ev)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }


            string simdi = Convert.ToString(DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString());

            if (Fotograf2 != null)
            {
                if (Fotograf2.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Content/img/yuklenenfotograflar"), string.Format(ev.EvID + "_" + simdi + ".jpg"));
                    Fotograf2.SaveAs(path);
                    ev.Fotograf = Convert.ToString(ev.EvID + "_" + simdi + ".jpg");
                }
            }

            if (ModelState.IsValid)
            {
                db.Ev.Add(ev);
                db.SaveChanges();
                return RedirectToAction("EvListe");
            }

            ViewBag.SehirID = new SelectList(db.Sehir, "SehirID", "SehirAdi", ev.SehirID);
            ViewBag.UyeID = new SelectList(db.Uye, "UyeID", "KullaniciAdi", ev.UyeID);
            return View(ev);
        }

        // GET: Admin/Edit/5
        public ActionResult EvDuzenle(int? id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ev ev = db.Ev.Find(id);
            if (ev == null)
            {
                return HttpNotFound();
            }
            ViewBag.SehirID = new SelectList(db.Sehir, "SehirID", "SehirAdi", ev.SehirID);
            ViewBag.UyeID = new SelectList(db.Uye, "UyeID", "KullaniciAdi", ev.UyeID);
            return View(ev);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EvDuzenle(HttpPostedFileBase Fotograf2, [Bind(Include = "EvID,Aciklama,IlanTarihi,EvinYasi,IlanTuru,KonutTipi,Fiyat,OdaSayisi,BanyoSayisi,Boyut,UyeID,SehirID,Fotograf")] Ev ev)
        {

            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }

            string simdi = Convert.ToString(DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString());

            if (Fotograf2 != null)
            {
                if (Fotograf2.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Content/img/yuklenenfotograflar"), string.Format(ev.EvID + "_" + simdi + ".jpg"));
                    Fotograf2.SaveAs(path);
                    ev.Fotograf = Convert.ToString(ev.EvID + "_" + simdi + ".jpg");
                }
            }

            if (ModelState.IsValid)
            {
                db.Entry(ev).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("EvDetay/" + ev.EvID);
            }
            ViewBag.SehirID = new SelectList(db.Sehir, "SehirID", "SehirAdi", ev.SehirID);
            ViewBag.UyeID = new SelectList(db.Uye, "UyeID", "KullaniciAdi", ev.UyeID);
            return View(ev);
        }

        // GET: Admin/Delete/5
        public ActionResult EvSil(int? id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ev ev = db.Ev.Find(id);
            if (ev == null)
            {
                return HttpNotFound();
            }
            return View(ev);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EvSilOnay(int id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            Ev ev = db.Ev.Find(id);
            db.Ev.Remove(ev);
            db.SaveChanges();
            return RedirectToAction("EvListe");
        }













        public ActionResult UyeListe()
        {
            return View(db.Uye.ToList());
        }

        // GET: Uyes/Details/5
        public ActionResult UyeDetay(int? id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uye uye = db.Uye.Find(id);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }



        // GET: Uyes/Edit/5
        public ActionResult UyeDuzenle(int? id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uye uye = db.Uye.Find(id);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }

        // POST: Uyes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UyeDuzenle([Bind(Include = "UyeID,KullaniciAdi,Sifresi,Email,UyelikTarihi,UyeAdi,Yetki")] Uye uye)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            if (ModelState.IsValid)
            {
                db.Entry(uye).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("UyeDetay/" + uye.UyeID);
            }
            return View(uye);
        }

        // GET: Uyes/Delete/5
        public ActionResult UyeSil(int? id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uye uye = db.Uye.Find(id);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }

        // POST: Uyes/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UyeSilOnay(int id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            Uye uye = db.Uye.Find(id);
            db.Uye.Remove(uye);
            db.SaveChanges();
            return RedirectToAction("UyeListe");
        }



        //
        //  return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());











        public ActionResult SliderListe()
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            return View(db.Slider.ToList());
        }



        public ActionResult SliderOlustur()
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            return View();
        }

        // POST: Sliders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SliderOlustur(HttpPostedFileBase Fotograf2, [Bind(Include = "sliderID,sliderLink,sliderAciklama")] Slider slider)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }




            string simdi = Convert.ToString(DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString());

            if (Fotograf2 != null)
            {
                if (Fotograf2.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Content/img/yuklenenfotograflar"), string.Format(slider.sliderID + "_" + simdi + ".jpg"));
                    Fotograf2.SaveAs(path);
                    slider.sliderLink = Convert.ToString(slider.sliderID + "_" + simdi + ".jpg");
                }
            }


            if (ModelState.IsValid)
            {
                db.Slider.Add(slider);
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            return View(slider);
        }










        // GET: Sliders/Details/5
        public ActionResult SliderDetay(int? id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Slider.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }


        // GET: Sliders/Edit/5
        public ActionResult SliderDuzenle(int? id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Slider.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // POST: Sliders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SliderDuzenle(HttpPostedFileBase Fotograf2, [Bind(Include = "sliderID,sliderLink,sliderAciklama")] Slider slider)
        {

            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }


            string simdi = Convert.ToString(DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString());

            if (Fotograf2 != null)
            {
                if (Fotograf2.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Content/img/yuklenenfotograflar"), string.Format(slider.sliderID + "_" + simdi + ".jpg"));
                    Fotograf2.SaveAs(path);
                    slider.sliderLink = Convert.ToString(slider.sliderID + "_" + simdi + ".jpg");
                }
            }

            if (ModelState.IsValid)
            {
                db.Entry(slider).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("SliderDuzenle/" + slider.sliderID);
            }
            return View(slider);
        }

        // GET: Sliders/Delete/5
        public ActionResult SliderSil(int? id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Slider.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // POST: Sliders/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SliderSilOnay(int id)
        {
            if (Session["uye_id"] != null)
            {
                if (int.Parse(Session["yetki"].ToString()) != 1)
                {
                    TempData["mesaj"] = "Yönetici Yetkiniz Bulunmamakta";
                    return RedirectToAction("Index", "Home", "");
                }
            }
            else
            {
                TempData["mesaj"] = "Yetki Bulunamadi";
                return RedirectToAction("Index", "Home", "");
            }

            Slider slider = db.Slider.Find(id);
            db.Slider.Remove(slider);
            db.SaveChanges();
            return RedirectToAction("SliderListe");
        }
























        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
