﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlueEstate.Models;
using System.Globalization;
using System.Data.SqlClient;

namespace BlueEstate.Controllers
{
    public class HomeController : Controller
    {

        private BlueEstateDBEntities db = new BlueEstateDBEntities();

        public ActionResult Index()
        {




            ViewBag.SehirID = new SelectList(db.Sehir, "SehirID", "SehirAdi");


            return View(new EvSlider(db));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpGet]
        public ActionResult Property(int id)
        {
            PropertyListe liste = new PropertyListe(db,id);

            
            return View(liste);
        }




        [HttpGet]
        public ActionResult Search( int SehirID= -1 , string  IlanTuru = "-1" , string  KonutTipi = "-1" , int  boyutt_min = -1, int  boyutt_max = -1 , int  Fiyat_min = 0 , int  Fiyat_max = -1 , int  OdaSayisi = -1 , int  BanyoSayisi = -1)
        {

            
            IlanlarSehirler bilgiler = new IlanlarSehirler(db);

            if (SehirID != -1)
            {
                bilgiler.ev = bilgiler.ev.Where(x => x.SehirID == SehirID).ToList();
            }
            if (KonutTipi != "-1")
            {
                bilgiler.ev = bilgiler.ev.Where(x => x.KonutTipi == KonutTipi).ToList();
            }
            if (IlanTuru != "-1")
            {
                bilgiler.ev = bilgiler.ev.Where(x => x.IlanTuru == IlanTuru).ToList();
            }
            if (boyutt_min != -1)
            {
                bilgiler.ev = bilgiler.ev.Where(x => x.Boyut > boyutt_min).ToList();
            }
            if (boyutt_max != -1)
            {
                bilgiler.ev = bilgiler.ev.Where(x => x.Boyut < boyutt_max).ToList();
            }
            if (OdaSayisi != -1)
            {
                bilgiler.ev = bilgiler.ev.Where(x => x.OdaSayisi == OdaSayisi).ToList();
            }
            if (Fiyat_max != -1)
            {
                bilgiler.ev = bilgiler.ev.Where(x => x.Fiyat < Fiyat_max).ToList();
            }
            if (Fiyat_min != -1)
            {
                bilgiler.ev = bilgiler.ev.Where(x => x.Fiyat > Fiyat_min).ToList();
            }

            if (BanyoSayisi != -1)
            {
                bilgiler.ev = bilgiler.ev.Where(x => x.BanyoSayisi == BanyoSayisi).ToList();
            }

            // var ev = db.Ev.Include(e => e.Sehir).Include(e => e.Uye);



            return View(bilgiler);
            
        }

       


        public ActionResult UyeGiris()
        {




            return View();
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            if (Session["dil"] == null)
            {
                Session["dil"] = "en";
            }
            if (Session["dil"].ToString() == "tr")
            {
                Session["dil"] = "en";
            }
            else
            {
                Session["dil"] = "tr";
            }

            Session["Culture"] = new CultureInfo(lang);

            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString()); 
        }


    }

    public class Katman
    {

        public LoginViewModel giris;
        public Uye kayit;


    }
    public class ARAMA
    {




        public int SehirID { get; set; }
        public int EvinYasi { get; set; }
        public string IlanTuru { get; set; }
        public string KonutTipi { get; set; }
        public string Fiyat_min { get; set; }
        public string Fiyat_max { get; set; }
        public int OdaSayisi { get; set; }
        public int boyutt_min { get; set; }
        public int boyutt_max { get; set; }
        public int BanyoSayisi { get; set; }




    }
}