﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlueEstate.Models;

namespace BlueEstate.Controllers
{
    public class UyeController : Controller
    {
        private BlueEstateDBEntities db = new BlueEstateDBEntities();

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult GirisYardim(LoginViewModel giris)
        {
            var uye = db.Uye.Where(x => x.Email == giris.Email && x.Sifresi == giris.Password).FirstOrDefault();
            if (uye != null)
            {
                Session["giris"] = true;
                Session["ad"] = uye.UyeAdi;
                Session["uye_id"] = uye.UyeID;
                Session["eposta"] = uye.Email;
                Session["yetki"] = uye.Yetki;

            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult KayitYardim(LoginViewModel giris)
        {

            Uye uye = new Uye();


            if (giris.Email != null && giris.Username != null && giris.Password != null)
            {

                uye.Email = giris.Email;
                uye.KullaniciAdi = giris.Username;
                uye.Sifresi = giris.Password;
                uye.Yetki = 0;
                uye.UyeAdi = "Adınızı Düzenleyin";
                uye.UyelikTarihi = DateTime.Now.ToString();


                db.Uye.Add(uye);
                db.SaveChanges();

                TempData["mesaj"] = "Kayıt başarılı";
            }
            else
            {
                TempData["mesaj"] = "Verilerinizi eksiksiz giriniz";
            }

            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());


        }



        // GET: Uyes
        public ActionResult Index()
        {
            return View(db.Uye.ToList());
        }

        // GET: Uyes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uye uye = db.Uye.Find(id);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }

        // GET: Uyes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Uyes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UyeID,KullaniciAdi,Sifresi,Email,UyelikTarihi,UyeAdi,Yetki")] Uye uye)
        {
            if (ModelState.IsValid)
            {
                db.Uye.Add(uye);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(uye);
        }

        // GET: Uyes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uye uye = db.Uye.Find(id);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }

        // POST: Uyes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UyeID,KullaniciAdi,Sifresi,Email,UyelikTarihi,UyeAdi,Yetki")] Uye uye)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uye).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(uye);
        }

        // GET: Uyes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uye uye = db.Uye.Find(id);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }

        // POST: Uyes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Uye uye = db.Uye.Find(id);
            db.Uye.Remove(uye);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
